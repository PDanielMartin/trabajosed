--TOP TESTBENCH
--BCD
--BUTTON_DEBOUNCE
--FLANC_DETECTOR
--SYNCHRNZR

--COMPONENT LIST FOR ENTITY TOP

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity in_proc is
Port (
    CLK:in std_logic;
    entrada:in std_logic;
    salida:out std_logic
 );
end in_proc;

architecture Behavioral of in_proc is                 
signal edge: std_logic;
signal sync_out: std_logic; 
   
component BUTTON_DEBOUNCE
Port(
    CLK: in std_logic;
    DATA_IN: in std_logic;
    DATA_OUT: out std_logic
);
end component;

component FLANC_DETECTOR
Port(
    CLK: in std_logic;
    SYNC_IN: in std_logic; 
    EDGE_OUT: out std_logic 
);
end component;

component SYNCHRNZR
Port(
    CLK: in std_logic;
    ASYNC_IN: in std_logic;
    SYNC_OUT: out std_logic

);
end component;

begin
    sync1: SYNCHRNZR port map(CLK,entrada,sync_out);
    edge1: FLANC_DETECTOR port map(CLK,sync_out,edge);
    debouncer1: BUTTON_DEBOUNCE port map(CLK,edge,salida);

end Behavioral;
