--Detector de flancos del reloj 
--Detecta Flancos Positivos
--Flanc Detector

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity FLANC_DETECTOR is
Port (
CLK: in std_logic; --reloj
SYNC_IN: in std_logic; 
EDGE_OUT: out std_logic --salida (1 si detecta flanco)
 );
end FLANC_DETECTOR;

architecture Behavioral of FLANC_DETECTOR is

SIGNAL sreg: std_logic_vector(2 downto 0);

begin
process(CLK)
begin
    if rising_edge(CLK) then
    sreg <= sreg (1 downto 0) & SYNC_IN;
    end if;
end process;

with sreg select

EDGE_OUT <= '1' when "001",
'0' when others;

end Behavioral;
