-- Manejo del rebote del boton
-- Button Debounce

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity BUTTON_DEBOUNCE is
 Port (
 CLK: in std_logic;
 DATA_IN: in std_logic;
 DATA_OUT: out std_logic
  );
end BUTTON_DEBOUNCE;

architecture Behavioral of BUTTON_DEBOUNCE is

signal operator1: std_logic;
signal operator2: std_logic;
signal operator3: std_logic;
-- 3 flipflops --> 3 ciclos de reloj
begin

process(CLK)
begin
    if rising_edge(CLK) then
    operator1 <= DATA_IN;
    operator2 <= operator1;
    operator3 <= operator2;
    end if;
end process;

DATA_OUT <= operator1 and not operator2 and not operator3;
end Behavioral;
