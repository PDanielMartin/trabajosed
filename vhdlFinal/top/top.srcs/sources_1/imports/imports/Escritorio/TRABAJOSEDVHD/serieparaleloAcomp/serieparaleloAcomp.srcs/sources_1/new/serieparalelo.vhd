library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity serieparalelo is
    port(
        CLK: in std_logic;
    
        SALIDACODIF : in std_logic_vector(3 downto 0);
        CLR: in std_logic;
        CE: in std_logic;
        
        SALIDASERIEPARALELO: out std_logic_vector(15 downto 0);
        READY: out std_logic
    );
end serieparalelo;

architecture Behavioral of serieparalelo is
    signal y: std_logic_vector(15 downto 0):="1111111111111111";
    signal cont: unsigned(2 downto 0):="000";
begin
    process(CLK)
    begin
        
        if CLK'event and clk='1' then
            if CLR='1' then
                y<=( others => '1');
                cont<="000";
            elsif CE='1' then
                if SALIDACODIF /= "1111" then
                    if cont=0 then
                        y<=SALIDACODIF & "111111111111";
                    elsif cont=1 then 
                        y<= y(15 downto 12)& SALIDACODIF & "11111111"; 
                    elsif cont=2 then 
                        y<= y(15 downto 8)& SALIDACODIF & "1111"; 
                    elsif cont=3 then 
                         y<= y(15 downto 4)& SALIDACODIF;  
                    end if;
                    
                    if(cont<4) then
                        cont<=1+cont;
                    end if;
                    
                 end if;
                 
                
                if cont =4 then
                    ready<='1';
                else
                    ready<='0';
                end if;
             end if;
         end if;
         SALIDASERIEPARALELO<=y;
     end process;
end Behavioral;
