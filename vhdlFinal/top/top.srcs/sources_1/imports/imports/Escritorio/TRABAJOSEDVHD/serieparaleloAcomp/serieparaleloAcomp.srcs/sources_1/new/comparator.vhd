library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity comparator is
 port(
    CLK: in std_logic;
    CE: in std_logic;
    ENTRADAUNO: in std_logic_vector(15 downto 0); --serieparalelo
    ENTRADADOS: in std_logic_vector(15 downto 0); --memoria
    OK: out std_logic
 );
 
end comparator;

architecture Behavioral of comparator is

begin
process(CLK)
    begin
    if rising_edge(CLK) then
        if CE ='1' then
            if ENTRADAUNO=ENTRADADOS then
                OK<='1';
            else 
                OK<='0';
            end if;
        else
            OK<='0';
        end if;
    end if;
end process;
end Behavioral;
