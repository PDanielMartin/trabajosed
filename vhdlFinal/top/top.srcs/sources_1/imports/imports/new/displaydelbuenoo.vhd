----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10.01.2022 20:42:07
-- Design Name: 
-- Module Name: displaydelbuenoo - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity control_display is
Port ( 
    codigo:in std_logic_vector(15 downto 0);
    EN:in std_logic;
    CLK:in std_logic;
    segmentos:out std_logic_vector(6 downto 0);
    digitos: out std_logic_vector(7 downto 0)
);
end entity;

architecture Behavioral of control_display is
    COMPONENT timer is
    Port ( 
    CE:IN STD_LOGIC;
    CLK: IN STD_LOGIC;
    TICK:OUT STD_LOGIC
    );
    end component;
    
    COMPONENT scanner is
    Port ( 
    CLK:IN STD_LOGIC;
    CE:IN STD_LOGIC;
    EN:IN STD_LOGIC;
    SAL: OUT std_logic_vector(3 DOWNTO 0)
    );
    end component;
    
    COMPONENT encoder is
    PORT (
    entrada : IN std_logic_vector(3 DOWNTO 0);
    binario : OUT std_logic_vector(1 DOWNTO 0)
    );
    end component;
    
    COMPONENT mux16_4 is
    Port ( 
        entrada: IN  STD_LOGIC_VECTOR (15 DOWNTO 0);
        sel:IN STD_LOGIC_VECTOR (1 DOWNTO 0);
        salida: OUT STD_LOGIC_VECTOR (3 DOWNTO 0)  
    );
    end component;
    
    COMPONENT decoder is
    PORT (
    code : IN std_logic_vector(3 DOWNTO 0);
    led : OUT std_logic_vector(6 DOWNTO 0)
    );
    end component;
    signal tick:std_logic;
    signal digitos_aux:std_logic_vector(3 downto 0);
    signal binario_aux:std_logic_vector(1 downto 0);
    signal codigo_aux:std_logic_vector(3 downto 0);
    
begin
    tim:timer port map(EN,CLK,tick);
    scan:scanner port map(CLK,tick,EN,digitos_aux);
    enco:encoder port map (digitos_aux,binario_aux);
    mux:mux16_4 port map (codigo,binario_aux,codigo_aux);
    decod:decoder port map(codigo_aux,segmentos);
    
    digitos<="1111"&digitos_aux;
  
    
end Behavioral;
