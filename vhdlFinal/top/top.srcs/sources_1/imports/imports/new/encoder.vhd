----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.ALL;
USE ieee.std_logic_unsigned.ALL;
ENTITY encoder IS
PORT (
entrada : IN std_logic_vector(3 DOWNTO 0);
binario : OUT std_logic_vector(1 DOWNTO 0)
);
END ENTITY encoder;

ARCHITECTURE dataflow OF encoder IS
BEGIN
WITH entrada SELECT
binario <= "00" WHEN "1110",
"01" WHEN "1101",
"10" WHEN "1011",
"11" WHEN "0111",
"00" WHEN others;
END ARCHITECTURE dataflow;