----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10.01.2022 21:22:45
-- Design Name: 
-- Module Name: scanner - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity scanner is
Port ( 
    CLK:IN STD_LOGIC;
    CE:IN STD_LOGIC;
    EN:IN STD_LOGIC;
    SAL: OUT std_logic_vector(3 DOWNTO 0)
);
end scanner;

architecture Behavioral of scanner is
signal SALaux:STD_LOGIC_VECTOR(SAL'range):="1110";
begin
    process(CLK,EN)
    begin
         SAL<=SALaux;
         if EN='0' then
                SAL<="1111";
         elsif rising_edge(CLK) then   
            if CE='1' then  
                SALaux<=SALaux(SAL'HIGH-1 DOWNTO 0) & SALaux(SAL'HIGH);
                
            end if;
         end if;
    end process;
end architecture;


