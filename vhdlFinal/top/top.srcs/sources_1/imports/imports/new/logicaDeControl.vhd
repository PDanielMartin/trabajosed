----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11.01.2022 18:15:05
-- Design Name: 
-- Module Name: logicaDeControl - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity logicaDeControl is
Port (
    CLK: in std_logic;

    ready: in std_logic;
    correcto: in std_logic;
    aceptar: in std_logic;
    cambiar: in std_logic;
    cerrar: in std_logic;
    
    aceptar2: out std_logic;
    cerrado2: out std_logic;
    cambio2: out std_logic;
    noAbierto: out std_logic;
    abierto: out std_logic;
    cerrado: out std_logic;
    cambio: out std_logic
    
);
end logicaDeControl;

architecture logicaDeControl of logicaDeControl is
    signal aceptar2_aux: std_logic;
    signal abierto_aux: std_logic;
    signal cerrado_aux: std_logic;
    signal cambio_aux: std_logic;
    
    COMPONENT fsm is
    port (
    CLK:in std_logic;
    aceptar2 : in std_logic;
    correcto : in std_logic;
    cambiar : in std_logic;
    cerrar : in std_logic;
    abierto : out std_logic;
    cerrado : out std_logic;
    cambio : out std_logic
    );
    end COMPONENT;
    
begin
    fsm1: fsm port map(CLK, aceptar2_aux,correcto,cambiar,cerrar,abierto_aux,cerrado_aux,cambio_aux);
    aceptar2_aux<=ready AND aceptar;
    cerrado2<=aceptar2_aux AND cerrado_aux;
    cambio2<=aceptar2_aux AND cambio_aux;
    noAbierto<= NOT abierto_aux;
    
    aceptar2<=aceptar2_aux;
    abierto<=abierto_aux;
    cerrado<=cerrado_aux;
    cambio<=cambio_aux;
    
    
    

end architecture;
