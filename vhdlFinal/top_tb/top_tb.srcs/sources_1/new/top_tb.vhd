----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.01.2022 21:44:31
-- Design Name: 
-- Module Name: top_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top_tb is

end top_tb;

architecture Behavioral of top_tb is
component top is
    Port ( 
    clk:in std_logic;
    BTN_NUMEROS:in std_logic_vector(9 downto 0);
    BTN_ACEPTAR:in std_logic;
    BTN_CAMBIAR:in std_logic;
    BTN_CERRAR:in std_logic;
    LED_ABIERTO:out std_logic;
    LED_CERRADO:out std_logic;
    LED_CAMBIO:out std_logic;
    SEGMENTOS:out std_logic_vector(6 downto 0);
    DIGITOS:out std_logic_vector(7 downto 0)  
);
end component;
    signal clk: std_logic;
    signal BTN_NUMEROS: std_logic_vector(9 downto 0):=(others =>'0');
    signal BTN_ACEPTAR: std_logic:='0';
    signal BTN_CAMBIAR: std_logic:='0';
    signal BTN_CERRAR: std_logic:='0';
    signal LED_ABIERTO: std_logic;
    signal LED_CERRADO: std_logic;
    signal LED_CAMBIO: std_logic;
    signal SEGMENTOS: std_logic_vector(6 downto 0);
    signal DIGITOS:std_logic_vector(7 downto 0);
    constant k:time:=10ns;  
begin
    uut: top port map(clk,BTN_NUMEROS,BTN_ACEPTAR,BTN_CAMBIAR,BTN_CERRAR,LED_ABIERTO,LED_CERRADO,LED_CAMBIO,SEGMENTOS,DIGITOS);
p0: process
begin
 CLK<='0';
 wait for 0.5*k;
 CLK<= '1';
 wait for 0.5*k;
 end process;
 
p1:process
begin

--wait for 2*k;
--BTN_NUMEROS(0)<='1';
--wait for 15*k;
--BTN_NUMEROS(0)<='0';
--wait for 2*k;
--BTN_NUMEROS(0)<='1';
--wait for 5*k;
--BTN_NUMEROS(0)<='0';
--wait for 2*k;
--BTN_NUMEROS(0)<='1';
--wait for 5*k;
--BTN_NUMEROS(0)<='0';
--wait for 2*k;
--BTN_NUMEROS(0)<='1';
--wait for 5*k;
--BTN_NUMEROS(0)<='0';

--wait for 6*k;
--BTN_ACEPTAR<='1';
--wait for 7*k;
--BTN_ACEPTAR<='0';

--wait for 3*k;
--BTN_CAMBIAR<='1';
--wait for 7*k;
--BTN_CAMBIAR<='0';

wait for 2*k;
BTN_NUMEROS(1)<='1';
wait for 6*k;
BTN_NUMEROS(1)<='0';
wait for 2*k;
BTN_NUMEROS(9)<='1';
wait for 5*k;
BTN_NUMEROS(9)<='0';
wait for 2*k;
BTN_NUMEROS(8)<='1';
wait for 5*k;
BTN_NUMEROS(8)<='0';
wait for 2*k;
BTN_NUMEROS(4)<='1';
wait for 5*k;
BTN_NUMEROS(4)<='0';

wait for 1000*k;

wait for 6*k;
BTN_ACEPTAR<='1';
wait for 7*k;
BTN_ACEPTAR<='0';

wait for 6*k;
BTN_CERRAR<='1';
wait for 7*k;
BTN_CERRAR<='0';

wait for 2*k;
BTN_NUMEROS(1)<='1';
wait for 6*k;
BTN_NUMEROS(1)<='0';
wait for 2*k;
BTN_NUMEROS(9)<='1';
wait for 5*k;
BTN_NUMEROS(9)<='0';
wait for 2*k;
BTN_NUMEROS(8)<='1';
wait for 5*k;
BTN_NUMEROS(8)<='0';
wait for 2*k;
BTN_NUMEROS(4)<='1';
wait for 5*k;
BTN_NUMEROS(4)<='0';

wait for 6*k;
BTN_ACEPTAR<='1';
wait for 7*k;
BTN_ACEPTAR<='0';

end process;
end Behavioral;
