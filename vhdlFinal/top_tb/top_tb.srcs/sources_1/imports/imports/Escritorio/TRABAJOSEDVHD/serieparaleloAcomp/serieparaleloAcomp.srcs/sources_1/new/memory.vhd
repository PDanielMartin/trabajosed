library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity memory is
  Port ( 
  ENTRADA: in std_logic_vector(15 downto 0);
  LOAD: in std_logic;
  clk: in std_logic;
  salida: out std_logic_vector(15 downto 0)
  );
end memory;

architecture Behavioral of memory is
signal mem:std_logic_vector(salida'range):=(others=>'0');
begin
process(CLK)
    begin
    if CLK'event and clk='1' then
        if LOAD ='1' then
            mem<=ENTRADA;
         end if; 
         salida<=mem;
    end if;
    
end process;
end Behavioral;
