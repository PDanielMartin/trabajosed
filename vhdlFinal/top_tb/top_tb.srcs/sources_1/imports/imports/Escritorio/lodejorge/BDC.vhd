--Conversor Binario Decimal
--Binary to Decimal Converter
--top number 9
--bottom number 0
--undefined result 10

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_arith.ALL;
use IEEE.std_logic_unsigned.ALL;


entity BDC is
Port ( 
    clk: in std_logic;
    decimal: in std_logic_vector(9 downto 0);
    binario: out std_logic_vector(3 downto 0)
);
end BDC;

architecture Behavioral of BDC is

begin
    process (clk) 
    begin
        case decimal is
        when "0000000001"  => binario <= "0000"; --0
        when "0000000010"  => binario <= "0001"; --1
        when "0000000100"  => binario <= "0010"; --2
        when "0000001000"  => binario <= "0011"; --3
        when "0000010000"  => binario <= "0100"; --4
        when "0000100000"  => binario <= "0101"; --5
        when "0001000000"  => binario <= "0110"; --6
        when "0010000000"  => binario <= "0111"; --7
        when "0100000000"  => binario <= "1000"; --8
        when "1000000000"  => binario <= "1001"; --9
        
        when others => binario <= "1111"; 
      end case;
    end process;
end Behavioral;
