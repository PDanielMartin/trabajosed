----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10.01.2022 20:55:29
-- Design Name: 
-- Module Name: mux16_4 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mux16_4 is
Port ( 
        entrada: IN  STD_LOGIC_VECTOR (15 DOWNTO 0);
        sel:IN STD_LOGIC_VECTOR (1 DOWNTO 0);
        salida: OUT STD_LOGIC_VECTOR (3 DOWNTO 0)  
 );
end mux16_4;

architecture Dataflow of mux16_4 is
    
begin
    WITH sel SELECT
    salida <= entrada (3 downto 0) WHEN "00",
    entrada (7 downto 4) WHEN "01",
    entrada (11 downto 8) WHEN "10",
    entrada (15 downto 12) WHEN "11",
    "1111" when others;
 END ARCHITECTURE dataflow;



