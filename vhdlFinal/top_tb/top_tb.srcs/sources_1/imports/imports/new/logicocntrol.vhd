----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11.01.2022 17:11:57
-- Design Name: 
-- Module Name: logicocntrol - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity fsm is
port (
    CLK:in std_logic;
    aceptar2 : in std_logic;
    correcto : in std_logic;
    cambiar : in std_logic;
    cerrar : in std_logic;
    abierto : out std_logic;
    cerrado : out std_logic;
    cambio : out std_logic
);
end fsm;

architecture behavioral of fsm is
 type STATES is (S_CERRADO, S_ABIERTO, S_CAMBIO);
 signal current_state: STATES := S_CERRADO;
 signal next_state: STATES;
begin
 state_register: process (CLK)
 begin
    if rising_edge(CLK) then    
        current_state<=next_state;
    end if;
 end process;
 
 nextstate_decod: process (correcto,cerrar,cambiar,aceptar2,current_state)
 begin
 next_state <= current_state;
 case current_state is
 when S_CERRADO =>
 if correcto = '1' then 
    next_state <= S_ABIERTO;
 end if;
 when S_ABIERTO =>
 
 if cerrar = '1' then 
    next_state <= S_CERRADO;

 elsif cambiar = '1' then 
    next_state <= S_CAMBIO;
 end if;
 
 when S_CAMBIO => 
 if aceptar2 = '1' then 
    next_state <=S_ABIERTO;
 end if;
 end case;
 end process;
 
 output_decod: process (current_state)
 begin
abierto<='0';
cerrado<='0';
cambio<='0';
 case current_state is
 when S_CERRADO =>
cerrado<= '1';
 when S_ABIERTO =>
 abierto<= '1';
 when S_CAMBIO=> 
 cambio<= '1';

 end case;
 end process;
end behavioral;
