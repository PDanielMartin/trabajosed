----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11.01.2022 20:21:53
-- Design Name: 
-- Module Name: control_display_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity control_display_tb is

end control_display_tb;

architecture Behavioral of control_display_tb is
    COMPONENT control_display is
    Port ( 
    codigo:in std_logic_vector(15 downto 0);
    EN:in std_logic;
    CLK:in std_logic;
    segmentos:out std_logic_vector(6 downto 0);
    digitos: out std_logic_vector(3 downto 0)
    );
    end component;
    signal codigo:std_logic_vector (15 downto 0);
    signal EN,CLK:std_logic;
    signal segmentos: std_logic_vector(6 downto 0);
    signal digitos:std_logic_vector(3 downto 0);
    constant k: time :=10ns;
begin
    uut: control_display port map(codigo,EN,CLK,segmentos,digitos);
    p0:process
    begin
        CLK<='0';
        wait for 0.5*k;
        CLK<= '1';
        wait for 0.5*k;
end process;
codigo<="0001100110000100";
EN<='0','1' after 10*k,'0' after 200*k, '1' after 230*k;


end Behavioral;
