----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10.01.2022 21:46:53
-- Design Name: 
-- Module Name: timer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity timer is
generic(modulo:positive:=5);
Port ( 
    CE:IN STD_LOGIC;
    CLK: IN STD_LOGIC;
    TICK:OUT STD_LOGIC
);
end timer;

architecture Behavioral of timer is

begin
    process(CLK)
        subtype count_t is natural range 0 to modulo;
        variable count:count_t;
        begin
            if rising_edge(CLK) and CE ='1' then
                count:=(count+1)mod modulo;
            end if;
            if count=modulo-1 then
                TICK<='1';
            else
                TICK<='0';
            end if;
    end process;
end architecture;
