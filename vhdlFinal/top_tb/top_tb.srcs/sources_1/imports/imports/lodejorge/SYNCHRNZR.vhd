--Sincronizador
--Synchronizer

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SYNCHRNZR is
Port ( 

CLK: in std_logic;
ASYNC_IN: in std_logic;
SYNC_OUT: out std_logic

);
end SYNCHRNZR;

architecture Behavioral of SYNCHRNZR is
signal SREG: std_logic_vector(1 downto 0);
begin

process (CLK)

begin
if rising_edge(CLK) then
SYNC_OUT <= SREG(1);
SREG <= sreg(0) & ASYNC_IN;
end if;

end process;

end Behavioral;
