----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.01.2022 20:18:56
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top is
Port ( 
    clk:in std_logic;
    BTN_NUMEROS:in std_logic_vector(9 downto 0);
    BTN_ACEPTAR:in std_logic;
    BTN_CAMBIAR:in std_logic;
    BTN_CERRAR:in std_logic;
    LED_ABIERTO:out std_logic;
    LED_CERRADO:out std_logic;
    LED_CAMBIO:out std_logic;
    SEGMENTOS:out std_logic_vector(6 downto 0);
    DIGITOS:out std_logic_vector(7 downto 0)  
);
end top;

architecture Behavioral of top is
    signal decimal:std_logic_vector(9 downto 0);
    signal binario: std_logic_vector(3 downto 0);
    signal paralelo:std_logic_vector(15 downto 0); 
    signal out_memoria:std_logic_vector(15 downto 0);
    signal ready:std_logic;
    signal correcto:std_logic;
    signal aceptar:std_logic;
    signal cambiar:std_logic;
    signal cerrar:std_logic;
    signal aceptar2:std_logic;
    signal cerrado2:std_logic;
    signal cambio2:std_logic;
    signal noAbierto:std_logic;
    
component logicaDeControl is
    Port (
    CLK: in std_logic;

    ready: in std_logic;
    correcto: in std_logic;
    aceptar: in std_logic;
    cambiar: in std_logic;
    cerrar: in std_logic;
    
    aceptar2: out std_logic;
    cerrado2: out std_logic;
    cambio2: out std_logic;
    noAbierto: out std_logic;
    abierto: out std_logic;
    cerrado: out std_logic;
    cambio: out std_logic
    
);
end component;

component in_proc
    Port (
    CLK:in std_logic;
    entrada:in std_logic;
    salida:out std_logic
 );
end component;

component control_display is
    Port ( 
    codigo:in std_logic_vector(15 downto 0);
    EN:in std_logic;
    CLK:in std_logic;
    segmentos:out std_logic_vector(6 downto 0);
    digitos: out std_logic_vector(7 downto 0)
);
end component;
component BDC is
  Port ( 
    clk: in std_logic;
    decimal: in std_logic_vector(9 downto 0);
    binario: out std_logic_vector(3 downto 0)
    );
end component;
component comparator is
 port(
    clk: in std_logic;
    CE: in std_logic;
    ENTRADAUNO: in std_logic_vector(15 downto 0); --serieparalelo
    ENTRADADOS: in std_logic_vector(15 downto 0); --memoria
    OK: out std_logic
 );
 end component;
 
 component memory is
  Port ( 
  ENTRADA: in std_logic_vector(15 downto 0);
  LOAD: in std_logic;
  clk: in std_logic;
  salida: out std_logic_vector(15 downto 0)
  );
 end component;
 
 component serieparalelo is
 port(
        SALIDACODIF : in std_logic_vector(3 downto 0);
        CLR: in std_logic;
        CE: in std_logic;
        CLK: in std_logic;
        SALIDASERIEPARALELO: out std_logic_vector(15 downto 0);
        READY: out std_logic
    );
 end component;

begin

gen:for i IN 0 to 9  generate
    inproc: in_proc port map(clk, BTN_NUMEROS(i),decimal(i));
end generate;
    bdc1: BDC port map(clk,decimal,binario);
    serpar1: serieparalelo port map(binario,aceptar2, noAbierto,clk,paralelo,ready);
    displa1: control_display port map(paralelo,noAbierto,clk,segmentos,digitos); 
    memory1: memory port map(paralelo,cambio2,clk,out_memoria);
    comp1: comparator port map(clk,cerrado2,paralelo,out_memoria,correcto);
    ldc:logicaDeControl port map(CLK,ready,correcto,aceptar,cambiar,cerrar,aceptar2,cerrado2,cambio2,noAbierto,LED_ABIERTO,LED_CERRADO,LED_CAMBIO);
    inprocACEPTAR: in_proc port map(clk,BTN_ACEPTAR,aceptar);
    inprocCAMBIAR: in_proc port map(clk,BTN_CAMBIAR,cambiar);
    inprocCERRAR: in_proc port map(clk,BTN_CERRAR,cerrar);
end Behavioral;
